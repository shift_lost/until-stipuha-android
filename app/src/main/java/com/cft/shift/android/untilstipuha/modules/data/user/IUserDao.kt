package com.cft.shift.android.untilstipuha.modules.data.user

import com.cft.shift.android.untilstipuha.models.User
import com.cft.shift.android.untilstipuha.models.network.responses.Me

interface IUserDao {

    interface IGetUsersCallback {
        fun onUsersLoaded(users: List<User>?)
    }

    interface IGetUserByIdCallback {
        fun onUserLoaded(user: User?)
    }

    interface IGetMeCallback {
        fun onMeLoaded(me: Me?)
    }

    fun getUsers(callback: IGetUsersCallback)

    fun getUserById(id: String, callback: IGetUserByIdCallback)

    fun getMe(callback: IGetMeCallback)
}