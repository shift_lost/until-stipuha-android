package com.cft.shift.android.untilstipuha.common.useCaseEngine

interface IUseCaseScheduler {
    fun execute(runnable: Runnable)

    fun <T: UseCase.ResponseValues> onResponse(response: T, callback: IUseCaseCallback<T>)

    fun <T: UseCase.ResponseValues> onError(callback: IUseCaseCallback<T>)
}