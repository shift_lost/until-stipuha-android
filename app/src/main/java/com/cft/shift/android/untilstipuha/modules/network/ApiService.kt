package com.cft.shift.android.untilstipuha.modules.network

import com.cft.shift.android.untilstipuha.models.Request
import com.cft.shift.android.untilstipuha.models.User
import com.cft.shift.android.untilstipuha.models.network.requests.LoginRequest
import com.cft.shift.android.untilstipuha.models.network.requests.RequestCreation
import com.cft.shift.android.untilstipuha.models.network.responses.LoginResponse
import com.cft.shift.android.untilstipuha.models.network.responses.Me
import okhttp3.Response
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface ApiService {
    companion object {
        const val USERS_PATH = "/users"
        const val REQUESTS_PATH = "/requests"
        const val SERVER_DOMAIN = "until-stepuha-server.herokuapp.com"
        const val SERVER_URL = "http://$SERVER_DOMAIN/"

        val instance: ApiService by lazy {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(
                    GsonConverterFactory.create())
                .baseUrl(SERVER_URL)
                .build()

            retrofit.create(ApiService::class.java)
        }
    }

    @POST("/login")
    fun loginUser(
        @Body loginRequest: LoginRequest
    ): Call<LoginResponse>

    @POST(REQUESTS_PATH)
    fun createRequest(
        @Header("WWW-Authenticate") token: String,
        @Body requestCreationRequest: RequestCreation
    ): Call<Request>

    @GET (REQUESTS_PATH)
    fun getRequestsList(): Call<List<Request>>

    @GET (REQUESTS_PATH)
    fun getRequestListByUserID(
        @Query("userId") userID: String
    ): Call<List<Request>>

    @GET ("$REQUESTS_PATH/{id}")
    fun getRequestByID(
        @Path("id") requestID: String
    ): Call<Request>

    @PATCH ("$REQUESTS_PATH/{id}")
    fun donateToRequest(
        @Header("WWW-Authenticate") token: String,
        @Path("id") requestID: String,
        @Body value: Double
    ): Call<ResponseBody>

    @GET (USERS_PATH)
    fun getUserList(): Call<List<User>>

    @GET ("$USERS_PATH/{id}")
    fun getUserInfo(
        @Path("id") userID: String
    ): Call<User>

    @DELETE ("$REQUESTS_PATH/{id}")
    fun deleteRequest(
        @Header("WWW-Authenticate") token: String,
        @Path("id") requestID: String
    ): Call<ResponseBody>

    @PATCH ("/balance")
    fun changeBalance(
        @Header("WWW-Authenticate") token: String,
        @Body value: Double
    ): Call<ResponseBody>

    @GET ("$USERS_PATH/{id}")
    fun getMyInfo(
        @Header("WWW-Authenticate") token: String,
        @Path("id") id: String
    ): Call<Me>


}