package com.cft.shift.android.untilstipuha.common

inline fun <T> T.guard(block: T.() -> Unit): T {
    if (this == null) block()
    return this
}
