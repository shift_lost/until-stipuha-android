package com.cft.shift.android.untilstipuha.models.network.requests

data class RequestCreation (
    val name: String,
    val description: String,
    val value: Double
)