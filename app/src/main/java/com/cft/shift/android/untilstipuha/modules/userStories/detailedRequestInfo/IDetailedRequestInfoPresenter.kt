package com.cft.shift.android.untilstipuha.modules.userStories.detailedRequestInfo

import com.cft.shift.android.untilstipuha.modules.userStories.base.IBasePresenter

interface IDetailedRequestInfoPresenter: IBasePresenter {
    val view: IDetailedRequestInfoView

    fun onDonate(value: Double)
}