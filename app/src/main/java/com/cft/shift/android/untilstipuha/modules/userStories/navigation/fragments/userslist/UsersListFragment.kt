package com.cft.shift.android.untilstipuha.modules.userStories.navigation.fragments.userslist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cft.shift.android.untilstipuha.models.User
import com.cft.shift.android.untilstipuha.modules.userStories.base.ListFragment
import es.dmoral.toasty.Toasty

class UsersListFragment: ListFragment(), IUsersListView {
    private lateinit var presenter: IUsersListPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view  = super.onCreateView(inflater, container, savedInstanceState)
        setPresenter(UsersListPresenter(this))

        return view
    }

    override fun onResume() {
        super.onResume()
        presenter.start()
    }

    override fun setPresenter(presenter: IUsersListPresenter) {
        this.presenter = presenter
    }

    override fun onListUsersLoaded(users: List<User>) {
        updateUsersList(users)
    }

    override fun onError() {
        context?.let { Toasty.error(it, "Failed to load list users", Toasty.LENGTH_SHORT).show() }
    }
}