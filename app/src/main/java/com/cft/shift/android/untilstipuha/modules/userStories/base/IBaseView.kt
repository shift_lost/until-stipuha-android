package com.cft.shift.android.untilstipuha.modules.userStories.base

interface IBaseView<in T: IBasePresenter> {
    fun setPresenter(presenter: T)
}