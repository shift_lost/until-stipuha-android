package com.cft.shift.android.untilstipuha.modules.useCases.request

import com.cft.shift.android.untilstipuha.common.useCaseEngine.UseCase
import com.cft.shift.android.untilstipuha.modules.data.Dao
import com.cft.shift.android.untilstipuha.modules.data.request.IRequestDao

class DonateRequest: UseCase<DonateRequest.RequestValues, DonateRequest.ResponseValues>() {
    override fun executeUseCase(requestValues: RequestValues) {
        if (requestValues.value < 0) {
            useCaseCallback.onError()
            return
        }
        Dao.instance.requestDao.donateRequestById(
            requestValues.requestId,
            requestValues.value,
            callback = object: IRequestDao.IDonateRequestByIdCallback {
                override fun onRequestDonated(success: Boolean) {
                    when {
                        success -> useCaseCallback.onSuccess(ResponseValues())
                        else -> useCaseCallback.onError()
                    }
                }
            })
    }

    class RequestValues(val requestId: String, val value: Double): UseCase.RequestValues

    class ResponseValues: UseCase.ResponseValues
}