package com.cft.shift.android.untilstipuha.modules.userStories.navigation.fragments.myself

import com.cft.shift.android.untilstipuha.models.Request
import com.cft.shift.android.untilstipuha.models.User
import com.cft.shift.android.untilstipuha.modules.userStories.base.IBasePresenter

interface IMyselfPresenter: IBasePresenter {
    val view: IMyselfView
    fun onDeposit(value: Double)
    fun onWithdraw(value: Double)
    fun onCloseRequest(request: Request)
    fun onLoadMyselfRequest()
    fun onLoadMyselfInfo()
}