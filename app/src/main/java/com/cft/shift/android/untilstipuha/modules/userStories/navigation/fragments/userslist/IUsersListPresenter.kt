package com.cft.shift.android.untilstipuha.modules.userStories.navigation.fragments.userslist

import com.cft.shift.android.untilstipuha.modules.userStories.base.IBasePresenter

interface IUsersListPresenter: IBasePresenter {
    val view: IUsersListView

    fun onLoadUsers()
}