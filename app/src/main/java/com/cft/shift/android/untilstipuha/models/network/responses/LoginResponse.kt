package com.cft.shift.android.untilstipuha.models.network.responses

data class LoginResponse (
    val id: String,
    val token: String
)