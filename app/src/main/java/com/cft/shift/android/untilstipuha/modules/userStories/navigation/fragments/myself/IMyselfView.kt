package com.cft.shift.android.untilstipuha.modules.userStories.navigation.fragments.myself

import com.cft.shift.android.untilstipuha.models.Request
import com.cft.shift.android.untilstipuha.models.User
import com.cft.shift.android.untilstipuha.models.network.responses.Me
import com.cft.shift.android.untilstipuha.modules.userStories.base.IBaseView

interface IMyselfView: IBaseView<IMyselfPresenter> {
    fun onBalanceChanged(newBalance: Int)
    fun onRequestAdded(request: Request)
    fun onListRequestsLoaded(requests: List<Request>)
    fun onUpdateViewsWithUser(user: Me)
    fun onSuccess(msg: String)
    fun onError(msg: String)
}