package com.cft.shift.android.untilstipuha.modules.userStories.navigation

import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.cft.shift.android.untilstipuha.R
import com.cft.shift.android.untilstipuha.modules.userStories.navigation.fragments.myself.MyselfFragment
import com.cft.shift.android.untilstipuha.modules.userStories.navigation.fragments.requestslist.RequestsListFragment
import com.cft.shift.android.untilstipuha.modules.userStories.navigation.fragments.userslist.UsersListFragment
import kotlinx.android.synthetic.main.activity_navigation.*

class NavigationActivity: AppCompatActivity() {
    private enum class Screen {
        REQUESTS, USERS, PROFILE
    }

    private var currentScreen = Screen.REQUESTS

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation)

        setupNavigationButton()
        setupFirstFragment()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putSerializable("screen", currentScreen)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        val state = savedInstanceState?.getSerializable("screen") as? Screen
        state?.let {
            when (it) {
                Screen.REQUESTS -> openRequestList()
                Screen.USERS -> openUserList()
                Screen.PROFILE -> openMyProfile()
            }
        }
    }

    private fun setupFirstFragment() {
        val fragment = supportFragmentManager.findFragmentById(R.id.fragment_layout)
        if (fragment == null)
            supportFragmentManager.beginTransaction()
                .add(R.id.fragment_layout, RequestsListFragment())
                .commit()
    }

    private fun changeFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                              .replace(R.id.fragment_layout, fragment)
                              .commit()
    }

    private fun setupNavigationButton() {
        navigationAllButton.setOnClickListener {
            openRequestList()
        }
        navigationUsersButton.setOnClickListener {
            openUserList()
        }
        navigationMyselfButton.setOnClickListener {
            openMyProfile()
        }
    }

    private fun openRequestList(){
        currentScreen = Screen.REQUESTS
        navigationButtonLayout.setBackgroundColor(Color.TRANSPARENT)
        navigationSelectAll.setBackgroundResource(R.color.colorPrimary)
        navigationSelectUsers.setBackgroundColor(Color.TRANSPARENT)
        navigationSelectSelf.setBackgroundColor(Color.TRANSPARENT)
        changeFragment(RequestsListFragment())
    }

    private fun openUserList(){
        currentScreen = Screen.USERS
        navigationButtonLayout.setBackgroundColor(Color.TRANSPARENT)
        navigationSelectAll.setBackgroundColor(Color.TRANSPARENT)
        navigationSelectUsers.setBackgroundResource(R.color.colorPrimary)
        navigationSelectSelf.setBackgroundColor(Color.TRANSPARENT)
        changeFragment(UsersListFragment())
    }

    private fun openMyProfile(){
        currentScreen = Screen.PROFILE
        navigationButtonLayout.setBackgroundResource(R.drawable.gradient_background)
        navigationSelectAll.setBackgroundColor(Color.TRANSPARENT)
        navigationSelectUsers.setBackgroundColor(Color.TRANSPARENT)
        navigationSelectSelf.setBackgroundResource(R.color.beige)
        changeFragment(MyselfFragment())
    }
}
