package com.cft.shift.android.untilstipuha.modules.useCases.request

import com.cft.shift.android.untilstipuha.common.useCaseEngine.UseCase
import com.cft.shift.android.untilstipuha.models.Request
import com.cft.shift.android.untilstipuha.modules.data.Dao
import com.cft.shift.android.untilstipuha.modules.data.request.IRequestDao

class GetRequestById: UseCase<GetRequestById.RequestValues, GetRequestById.ResponseValues>() {
    override fun executeUseCase(requestValues: RequestValues) {
        Dao.instance.requestDao.getRequestById(
            requestId = requestValues.requestId,
            callback = object: IRequestDao.IGetRequestCallback {
                override fun onRequestLoaded(request: Request?) {
                    when {
                        request != null -> useCaseCallback.onSuccess(
                            ResponseValues(
                                request
                            )
                        )
                        else -> useCaseCallback.onError()
                    }
                }
            }
        )
    }

    class RequestValues(val requestId: String): UseCase.RequestValues

    class ResponseValues(val request: Request): UseCase.ResponseValues
}