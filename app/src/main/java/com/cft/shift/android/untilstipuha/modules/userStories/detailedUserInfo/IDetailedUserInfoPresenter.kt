package com.cft.shift.android.untilstipuha.modules.userStories.detailedUserInfo

import com.cft.shift.android.untilstipuha.modules.userStories.base.IBasePresenter

interface IDetailedUserInfoPresenter: IBasePresenter {
    val view: IDetailedUserInfoView

    fun onLoadDetailedRequest()
    fun onLoadDetailedInfo()
}