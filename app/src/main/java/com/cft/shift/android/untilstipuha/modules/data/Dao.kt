package com.cft.shift.android.untilstipuha.modules.data

import com.cft.shift.android.untilstipuha.modules.data.balance.IBalanceDao
import com.cft.shift.android.untilstipuha.modules.data.balance.NetworkBalanceDao
import com.cft.shift.android.untilstipuha.modules.data.login.ILoginDao
import com.cft.shift.android.untilstipuha.modules.data.login.NetworkLoginDao
import com.cft.shift.android.untilstipuha.modules.data.request.IRequestDao
import com.cft.shift.android.untilstipuha.modules.data.request.NetworkRequestDao
import com.cft.shift.android.untilstipuha.modules.data.user.IUserDao
import com.cft.shift.android.untilstipuha.modules.data.user.NetworkUserDao

class Dao private constructor(
    val loginDao: ILoginDao,
    val userDao: IUserDao,
    val requestDao: IRequestDao,
    val balanceDao: IBalanceDao
) {
    private object Holder {
        val INSTANCE = Dao(NetworkLoginDao(), NetworkUserDao(), NetworkRequestDao(), NetworkBalanceDao())
    }

    companion object {
        val instance by lazy { Holder.INSTANCE }
    }
}