package com.cft.shift.android.untilstipuha.models.network.responses

data class Me (
    val id: String,
    val name: String,
    val karma: Int,
    val maxRequest: Double,
    val balance: Double
)