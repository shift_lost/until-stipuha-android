package com.cft.shift.android.untilstipuha.models

data class User(
    val id: String,
    val name: String,
    val karma: Int,
    val maxRequest: Double
)