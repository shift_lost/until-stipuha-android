package com.cft.shift.android.untilstipuha.modules.useCases.balance

import com.cft.shift.android.untilstipuha.common.useCaseEngine.UseCase
import com.cft.shift.android.untilstipuha.modules.data.Dao
import com.cft.shift.android.untilstipuha.modules.data.balance.IBalanceDao

class Withdraw: UseCase<Withdraw.RequestValues, Withdraw.ResponseValues>() {
    override fun executeUseCase(requestValues: RequestValues) {
        if (requestValues.value <= 0)
            useCaseCallback.onError()
        Dao.instance.balanceDao.withdraw(
            requestValues.value,
            callback = object: IBalanceDao.IWithdrawCallback {
                override fun onWithdrew(success: Boolean) {
                    when {
                        success -> useCaseCallback.onSuccess(ResponseValues())
                        else -> useCaseCallback.onError()
                    }
                }
            }
        )
    }

    class RequestValues(val value: Double): UseCase.RequestValues

    class ResponseValues: UseCase.ResponseValues
}