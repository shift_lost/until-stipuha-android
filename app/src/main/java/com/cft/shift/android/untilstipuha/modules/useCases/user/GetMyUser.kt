package com.cft.shift.android.untilstipuha.modules.useCases.user

import com.cft.shift.android.untilstipuha.common.useCaseEngine.UseCase
import com.cft.shift.android.untilstipuha.models.User
import com.cft.shift.android.untilstipuha.models.network.responses.Me
import com.cft.shift.android.untilstipuha.modules.data.AuthInfoContainer
import com.cft.shift.android.untilstipuha.modules.data.Dao
import com.cft.shift.android.untilstipuha.modules.data.user.IUserDao

class GetMyUser: UseCase<GetMyUser.RequestValues, GetMyUser.ResponseValues>() {
    override fun executeUseCase(requestValues: RequestValues) {
        Dao.instance.userDao.getMe(object: IUserDao.IGetMeCallback {
            override fun onMeLoaded(me: Me?) {
                if (me != null) {
                    useCaseCallback.onSuccess(ResponseValues(me))
                } else {
                    useCaseCallback.onError()
                }
            }
        })
    }

    class RequestValues: UseCase.RequestValues

    class ResponseValues(val me: Me): UseCase.ResponseValues
}