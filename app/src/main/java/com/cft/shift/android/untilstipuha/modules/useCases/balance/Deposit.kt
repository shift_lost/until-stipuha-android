package com.cft.shift.android.untilstipuha.modules.useCases.balance

import com.cft.shift.android.untilstipuha.common.useCaseEngine.UseCase
import com.cft.shift.android.untilstipuha.modules.data.Dao
import com.cft.shift.android.untilstipuha.modules.data.balance.IBalanceDao

class Deposit: UseCase<Deposit.RequestValues, Deposit.ResponseValues>() {
    override fun executeUseCase(requestValues: RequestValues) {
        if (requestValues.value <= 0)
            useCaseCallback.onError()
        Dao.instance.balanceDao.deposit(
            requestValues.value,
            callback = object: IBalanceDao.IDepositCallback {
                override fun onDeposited(success: Boolean) {
                    when {
                        success -> useCaseCallback.onSuccess(Deposit.ResponseValues())
                        else -> useCaseCallback.onError()
                    }
                }
            }
        )
    }

    class RequestValues(val value: Double): UseCase.RequestValues

    class ResponseValues: UseCase.ResponseValues
}