package com.cft.shift.android.untilstipuha.models.network.requests

data class LoginRequest (
    val name: String,
    val password: String
)