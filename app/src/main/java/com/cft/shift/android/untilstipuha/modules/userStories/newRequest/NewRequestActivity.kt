package com.cft.shift.android.untilstipuha.modules.userStories.newRequest

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.cft.shift.android.untilstipuha.R
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.activity_create_request.*

class NewRequestActivity: AppCompatActivity(), INewRequestView {
    private lateinit var presenter: INewRequestPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_request)
        setPresenter(NewRequestPresenter(this))

        requestCreateButton.setOnClickListener {
            val requestName = requestTitleField.text.toString()
            val aim = requestSum.text.toString().toDoubleOrNull()
            val description = requestDescriptionField.text.toString()
            if (!requestName.isBlank() && aim != null && !description.isBlank()) {
                presenter.onCreateRequest(requestName, aim, description)
            } else {
                Toasty.error(this, "Data in fields is not valid!", Toasty.LENGTH_SHORT).show()
            }
        }
    }

    override fun onRequestAdded() {
        Toasty.success(this, "Request successfully added!", Toasty.LENGTH_SHORT).show()
        finish()
    }

    override fun onError() {
        Toasty.error(this, "Cannot add request:(", Toasty.LENGTH_SHORT).show()
    }

    override fun setPresenter(presenter: INewRequestPresenter) {
        this.presenter = presenter
    }
}