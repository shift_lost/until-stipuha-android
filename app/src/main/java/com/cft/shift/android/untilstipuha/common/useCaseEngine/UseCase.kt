package com.cft.shift.android.untilstipuha.common.useCaseEngine

abstract class UseCase<RequestType: UseCase.RequestValues, ResponseType: UseCase.ResponseValues> {
    lateinit var requestValues: RequestType
    lateinit var useCaseCallback: IUseCaseCallback<ResponseType>

    //region Public interface
    fun run(){
        executeUseCase(requestValues)
    }
    //endregion

    //region Should be overridden
    protected abstract fun executeUseCase(requestValues: RequestType)
    //endregion

    //region Internal types for Request and Response values
    interface RequestValues
    interface ResponseValues
    //endregion
}