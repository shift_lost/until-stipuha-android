package com.cft.shift.android.untilstipuha.modules.useCases.request

import com.cft.shift.android.untilstipuha.common.useCaseEngine.UseCase
import com.cft.shift.android.untilstipuha.models.Request
import com.cft.shift.android.untilstipuha.modules.data.Dao
import com.cft.shift.android.untilstipuha.modules.data.request.IRequestDao

class AddRequest: UseCase<AddRequest.RequestValues, AddRequest.ResponseValues>() {
    override fun executeUseCase(requestValues: RequestValues) {
        Dao.instance.requestDao.addRequest(
            requestValues.requestName,
            requestValues.description,
            requestValues.aim,
            callback = object: IRequestDao.IAddRequestCallback {
                override fun onRequestAdded(request: Request?) {
                    when {
                        request != null -> useCaseCallback.onSuccess(ResponseValues(request))
                        else -> useCaseCallback.onError()
                    }
                }
            }
        )
    }

    class RequestValues(val requestName: String, val description: String, val aim: Double): UseCase.RequestValues

    class ResponseValues(val request: Request): UseCase.ResponseValues
}