package com.cft.shift.android.untilstipuha.modules.userStories.detailedRequestInfo

import com.cft.shift.android.untilstipuha.models.Request
import com.cft.shift.android.untilstipuha.modules.userStories.base.IBaseView

interface IDetailedRequestInfoView: IBaseView<IDetailedRequestInfoPresenter> {
    fun getRequestId(): String
    fun onUpdateViewsWithRequest(request: Request)

    fun onError()
}