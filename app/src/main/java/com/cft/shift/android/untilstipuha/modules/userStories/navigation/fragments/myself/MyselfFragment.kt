package com.cft.shift.android.untilstipuha.modules.userStories.navigation.fragments.myself

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.cft.shift.android.untilstipuha.R
import com.cft.shift.android.untilstipuha.models.Request
import com.cft.shift.android.untilstipuha.models.network.responses.Me
import com.cft.shift.android.untilstipuha.modules.userStories.base.BaseFragment
import com.cft.shift.android.untilstipuha.modules.userStories.newRequest.NewRequestActivity
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.donate_dialog_box.*
import kotlinx.android.synthetic.main.fragment_view_self_profile.*
import kotlinx.android.synthetic.main.fragment_view_self_profile.view.*
import kotlinx.android.synthetic.main.item_self_request.view.*
import kotlin.math.roundToInt

class MyselfFragment: BaseFragment(R.layout.fragment_view_self_profile), IMyselfView {
    private lateinit var presenter: IMyselfPresenter
    private lateinit var list: RecyclerView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view  = super.onCreateView(inflater, container, savedInstanceState)
        setPresenter(MyselfPresenter(this))

        list = view!!.self_request_list
        list.layoutManager = LinearLayoutManager(activity)

        view.myselfViewCreateNewRequestButton.setOnClickListener {
            val intent = Intent(context, NewRequestActivity::class.java)
            this.context?.startActivity(intent)
        }
        view.myselfViewDepositMoneyButton.setOnClickListener {
            deposit()
        }
        view.myselfViewWithdrawMoneyButton.setOnClickListener {
            withdraw()
        }

        return view
    }

    override fun onResume() {
        super.onResume()
        presenter.start()
    }

    override fun onBalanceChanged(newBalance: Int) {
        myselfViewBalanceText.setText(newBalance)
    }

    override fun onRequestAdded(request: Request) {
        (list.adapter as MyselfRequestsListAdapter).requests.plus(request)
    }

    override fun setPresenter(presenter: IMyselfPresenter) {
        this.presenter = presenter
    }

    override fun onListRequestsLoaded(requests: List<Request>) {
        list.adapter = context?.let { MyselfRequestsListAdapter(requests) }
    }

    override fun onUpdateViewsWithUser(user: Me) {
        view?.let {
            it.myselfViewUsername.text = user.name
            it.myselfViewKarmaText.text = user.karma.toString()
            it.myselfViewMaxRequestText.text = user.maxRequest.roundToInt().toString()
            it.myselfViewBalanceText.text = user.balance.toString()
        }
    }

    override fun onSuccess(msg: String) {
        context?.let {
            Toasty.success(it, msg, Toasty.LENGTH_SHORT).show()
        }
    }

    override fun onError(msg: String) {
        context?.let {
            Toasty.error(it, msg, Toasty.LENGTH_SHORT).show()
        }
    }

    private fun deposit(){
        val layoutInflaterAndroid = LayoutInflater.from(context!!)
        val v = layoutInflaterAndroid.inflate(R.layout.donate_dialog_box, null)
        val alertDialogBuilderUserInput = AlertDialog.Builder(context!!, R.style.AlertDialogTheme)

        val donate = v.findViewById<EditText>(R.id.donateValue)
        alertDialogBuilderUserInput.setView(v)

        alertDialogBuilderUserInput
            .setCancelable(false)
            .setPositiveButton(R.string.confirm) { _, _ ->
                presenter.onDeposit(donate.text.toString().toDouble())
            }
            .setNegativeButton(R.string.cancel) { _, _ ->
            }.show()
    }

    private fun withdraw(){
        val layoutInflaterAndroid = LayoutInflater.from(context!!)
        val v = layoutInflaterAndroid.inflate(R.layout.donate_dialog_box, null)
        val alertDialogBuilderUserInput = AlertDialog.Builder(context!!, R.style.AlertDialogTheme)

        val donate = v.findViewById<EditText>(R.id.donateValue)
        alertDialogBuilderUserInput.setView(v)

        alertDialogBuilderUserInput
            .setCancelable(false)
            .setPositiveButton(R.string.confirm) { _, _ ->
                presenter.onWithdraw(donate.text.toString().toDouble())
            }
            .setNegativeButton(R.string.cancel) { _, _ ->
            }.show()
    }

    inner class MyselfRequestsListAdapter(val requests: List<Request>):
        RecyclerView.Adapter<RequestHolder>() {

        override fun onCreateViewHolder(viewGroup: ViewGroup, pos: Int): RequestHolder {
            return RequestHolder(LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.item_self_request, viewGroup, false))
        }

        override fun getItemCount(): Int {
            return requests.size
        }

        override fun onBindViewHolder(requestHolder: RequestHolder, pos: Int) {
            requestHolder.title.text = requests[pos].name
            requestHolder.balance.text = requests[pos].balance.toString()
            requestHolder.aim.text = requests[pos].aim.toString()
            requestHolder.delete.setOnClickListener{
                presenter.onCloseRequest(requests[pos])
            }
        }
    }

    inner class RequestHolder(view: View): RecyclerView.ViewHolder(view) {
        val title = view.itemSelfRequestTitle
        val balance = view.itemSelfRequestSum
        val aim = view.itemSelfRequestNeed
        val delete = view.itemSelfRequestRemoveButton
    }
}