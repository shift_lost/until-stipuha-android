package com.cft.shift.android.untilstipuha.modules.data.login

interface ILoginDao {

    interface ITryLoginCallback {
        fun onLogin(id: String?, token: String?)
    }

    fun tryLogin(name: String, password: String, callback: ITryLoginCallback)
}