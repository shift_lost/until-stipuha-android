package com.cft.shift.android.untilstipuha.modules.userStories.navigation.fragments.userslist

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cft.shift.android.untilstipuha.R
import com.cft.shift.android.untilstipuha.common.SharedConstants
import com.cft.shift.android.untilstipuha.models.User
import com.cft.shift.android.untilstipuha.modules.userStories.detailedUserInfo.DetailedUserInfoActivity
import kotlinx.android.synthetic.main.item_user.view.*

class UsersListAdapter(private val users: List<User>, private val context: Context):
    RecyclerView.Adapter<UsersListAdapter.UserHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, pos: Int): UserHolder {
        return UserHolder(LayoutInflater.from(viewGroup.context)
                                           .inflate(R.layout.item_user, viewGroup, false))
    }

    override fun getItemCount(): Int {
        return users.size
    }

    override fun onBindViewHolder(userHolder: UserHolder, pos: Int) {
        userHolder.name.text = users[pos].name

        userHolder.itemView.setOnClickListener {
            val intent = Intent(context, DetailedUserInfoActivity::class.java)
            intent.putExtra(SharedConstants.userIdTag, users[pos].id)
            context.startActivity(intent)
        }
    }


    inner class UserHolder(view: View): RecyclerView.ViewHolder(view) {
        val name = view.username
    }
}