package com.cft.shift.android.untilstipuha.modules.userStories.detailedRequestInfo

import com.cft.shift.android.untilstipuha.common.useCaseEngine.IUseCaseCallback
import com.cft.shift.android.untilstipuha.common.useCaseEngine.UseCaseHandler
import com.cft.shift.android.untilstipuha.models.Request
import com.cft.shift.android.untilstipuha.modules.useCases.request.CloseRequest
import com.cft.shift.android.untilstipuha.modules.useCases.request.DonateRequest
import com.cft.shift.android.untilstipuha.modules.useCases.request.GetRequestById

class DetailedRequestInfoPresenter(override val view: IDetailedRequestInfoView) : IDetailedRequestInfoPresenter {
    private lateinit var request: Request

    override fun start() {
        val requestId = view.getRequestId()
        val getRequestById = GetRequestById()
        val getRequestByIdRequestParams = GetRequestById.RequestValues(requestId)

        UseCaseHandler.instance.execute(
            getRequestById,
            getRequestByIdRequestParams,
            object: IUseCaseCallback<GetRequestById.ResponseValues> {
                override fun onSuccess(response: GetRequestById.ResponseValues) {
                    request = response.request
                    view.onUpdateViewsWithRequest(response.request)
                }
                override fun onError() {
                    view.onError()
                }
            }
        )
    }

    override fun onDonate(value: Double) {
        try {
            val donate = DonateRequest()
            val reqVals = DonateRequest.RequestValues(request.id, value)
            UseCaseHandler.instance.execute(
                donate,
                reqVals,
                object : IUseCaseCallback<DonateRequest.ResponseValues> {
                    override fun onSuccess(response: DonateRequest.ResponseValues) {
                        start()
                    }
                    override fun onError() {
                    }
                }
            )
        } catch (ex: UninitializedPropertyAccessException){}
    }
}