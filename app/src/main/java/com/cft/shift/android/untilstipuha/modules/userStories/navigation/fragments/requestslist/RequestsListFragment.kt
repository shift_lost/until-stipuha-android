package com.cft.shift.android.untilstipuha.modules.userStories.navigation.fragments.requestslist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cft.shift.android.untilstipuha.models.Request
import com.cft.shift.android.untilstipuha.modules.userStories.base.ListFragment
import es.dmoral.toasty.Toasty
import java.util.*

class RequestsListFragment: ListFragment(), IRequestsListView {
    private lateinit var presenter: IRequestsListPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view  = super.onCreateView(inflater, container, savedInstanceState)
        setPresenter(RequestsListPresenter(this))

        return view
    }

    override fun onResume() {
        super.onResume()
        presenter.start()
    }

    override fun setPresenter(presenter: IRequestsListPresenter) {
        this.presenter = presenter
    }

    override fun onListRequestsLoaded(requests: List<Request>) {
        updateRequestsList(requests)
    }

    override fun onError() {
        context?.let { Toasty.error(it, "failed to load list requests", Toasty.LENGTH_SHORT).show() }
    }
}