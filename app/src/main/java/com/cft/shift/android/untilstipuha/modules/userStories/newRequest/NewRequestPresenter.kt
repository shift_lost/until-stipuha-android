package com.cft.shift.android.untilstipuha.modules.userStories.newRequest

import android.util.Log
import com.cft.shift.android.untilstipuha.common.useCaseEngine.IUseCaseCallback
import com.cft.shift.android.untilstipuha.common.useCaseEngine.UseCaseHandler
import com.cft.shift.android.untilstipuha.modules.useCases.request.AddRequest

class NewRequestPresenter(override val view: INewRequestView) :  INewRequestPresenter {
    override fun onCreateRequest(name: String, aim: Double, description: String) {
        val addRequest = AddRequest()
        val requestValues = AddRequest.RequestValues(name, description, aim)
        UseCaseHandler.instance.execute(
            addRequest,
            requestValues,
            object: IUseCaseCallback<AddRequest.ResponseValues> {
                override fun onSuccess(response: AddRequest.ResponseValues) {
                    Log.d("NewRequestPresenter", response.request.toString())
                    view.onRequestAdded()
                }
                override fun onError() {
                    view.onError()
                }
            }
        )
    }

    override fun start() {}
}