package com.cft.shift.android.untilstipuha.modules.userStories.detailedRequestInfo

import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.widget.EditText
import com.cft.shift.android.untilstipuha.R
import com.cft.shift.android.untilstipuha.common.SharedConstants
import com.cft.shift.android.untilstipuha.models.Request
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.activity_view_request.*

class DetailedRequestInfoActivity: AppCompatActivity(), IDetailedRequestInfoView {
    private lateinit var presenter: IDetailedRequestInfoPresenter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_request)
        setPresenter(DetailedRequestInfoPresenter(this))

        donateButton.setOnClickListener {
            val layoutInflaterAndroid = LayoutInflater.from(this)
            val v = layoutInflaterAndroid.inflate(R.layout.donate_dialog_box, null)
            val alertDialogBuilderUserInput = AlertDialog.Builder(this, R.style.AlertDialogTheme)
            alertDialogBuilderUserInput.setView(v)

            alertDialogBuilderUserInput
                .setCancelable(false)
                .setPositiveButton(R.string.confirm) { _, _ ->
                    val donate = v.findViewById<EditText>(R.id.donateValue)
                    presenter.onDonate(donate.text.toString().toDouble())
                }
                .setNegativeButton(R.string.cancel) { _, _ ->
                }.show()
        }

        presenter.start()
    }

    override fun onUpdateViewsWithRequest(request: Request) {
        requestTitle.text = request.name
        requestCurrentSum.text = request.balance.toString()
        requestNeededSum.text = request.aim.toString()

        requestPercent.text = "${((request.balance / request.aim)*100).toInt().toString()}%"
        requestProgressBar.max = request.aim.toInt()
        requestProgressBar.setProgress(request.balance.toInt(), true)
        requestDescription.text = request.description
    }

    override fun onError() {
        Toasty.error(this, "Cannot load this request").show()
    }

    override fun getRequestId(): String {
        return intent.getStringExtra(SharedConstants.requestIdTag)
    }

    override fun setPresenter(presenter: IDetailedRequestInfoPresenter) {
        this.presenter = presenter
    }
}