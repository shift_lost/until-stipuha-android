package com.cft.shift.android.untilstipuha.modules.data.balance

import com.cft.shift.android.untilstipuha.modules.data.AuthInfoContainer
import com.cft.shift.android.untilstipuha.modules.data.validateToken
import com.cft.shift.android.untilstipuha.modules.network.ApiService
import java.io.IOException

class NetworkBalanceDao: IBalanceDao {
    override fun withdraw(value: Double, callback: IBalanceDao.IWithdrawCallback) {
        AuthInfoContainer.validateToken {
            callback.onWithdrew(false)
            return
        }

        try {
            val response = ApiService.instance.changeBalance(AuthInfoContainer.token, value).execute()
            callback.onWithdrew(response.isSuccessful)
        } catch (e: IOException) {
            callback.onWithdrew(false)
        }
    }

    override fun deposit(value: Double, callback: IBalanceDao.IDepositCallback) {
        AuthInfoContainer.validateToken {
            callback.onDeposited(false)
            return
        }

        try {
            val response = ApiService.instance.changeBalance(AuthInfoContainer.token, -value).execute()
            callback.onDeposited(response.isSuccessful)
        } catch (e: IOException) {
            callback.onDeposited(false)
        }
    }
}