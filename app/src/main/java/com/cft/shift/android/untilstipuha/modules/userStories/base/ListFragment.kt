package com.cft.shift.android.untilstipuha.modules.userStories.base

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cft.shift.android.untilstipuha.R
import com.cft.shift.android.untilstipuha.models.Request
import com.cft.shift.android.untilstipuha.models.User
import com.cft.shift.android.untilstipuha.modules.userStories.navigation.fragments.requestslist.RequestsListAdapter
import com.cft.shift.android.untilstipuha.modules.userStories.navigation.fragments.userslist.UsersListAdapter
import kotlinx.android.synthetic.main.fragment_recycler_view.view.*

abstract class ListFragment : BaseFragment(R.layout.fragment_recycler_view) {
    protected lateinit var list: RecyclerView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        list = view!!.recyclerViewList
        list.layoutManager = LinearLayoutManager(activity)

        return view
    }

    protected fun updateRequestsList(requests: List<Request>) {
        list.adapter = context?.let { RequestsListAdapter(requests, it) }
    }

    protected fun updateUsersList(users: List<User>) {
        list.adapter = context?.let { UsersListAdapter(users, it) }
    }
}