package com.cft.shift.android.untilstipuha.modules.userStories.detailedUserInfo

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cft.shift.android.untilstipuha.R
import com.cft.shift.android.untilstipuha.common.SharedConstants
import com.cft.shift.android.untilstipuha.models.Request
import com.cft.shift.android.untilstipuha.modules.userStories.detailedRequestInfo.DetailedRequestInfoActivity
import kotlinx.android.synthetic.main.item_request.view.*
import kotlinx.android.synthetic.main.item_self_request.view.*

class DetailedRequestsListAdapter(val requests: List<Request>, private val context: Context):
    RecyclerView.Adapter<DetailedRequestsListAdapter.RequestHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, pos: Int): RequestHolder {
        return RequestHolder(LayoutInflater.from(viewGroup.context)
                                           .inflate(R.layout.item_request, viewGroup, false))
    }

    override fun getItemCount(): Int {
        return requests.size
    }

    override fun onBindViewHolder(requestHolder: RequestHolder, pos: Int) {
        requestHolder.title.text = requests[pos].name
        requestHolder.balance.text = requests[pos].balance.toString()
        requestHolder.aim.text = requests[pos].aim.toString()

        requestHolder.itemView.setOnClickListener {
            val intent = Intent(context, DetailedRequestInfoActivity::class.java)
            intent.putExtra(SharedConstants.requestIdTag, requests[pos].id)
            context.startActivity(intent)
        }
    }


    inner class RequestHolder(view: View): RecyclerView.ViewHolder(view) {
        val title = view.itemRequestTitle
        val balance = view.itemRequestSum
        val aim = view.itemRequestNeed
    }
}