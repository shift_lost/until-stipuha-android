package com.cft.shift.android.untilstipuha.common.useCaseEngine

class UseCaseCallbackWrapper<T: UseCase.ResponseValues>(
    private val callback: IUseCaseCallback<T>,
    private val useCaseHandler: UseCaseHandler
    ): IUseCaseCallback<T> {

    //region IUseCaseCallback
    override fun onSuccess(response: T) {
        useCaseHandler.notifyResponse(response, callback)
    }

    override fun onError() {
        useCaseHandler.notifyError(callback)
    }
    //endregion

}