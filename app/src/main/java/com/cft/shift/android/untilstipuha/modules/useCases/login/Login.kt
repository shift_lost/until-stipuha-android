package com.cft.shift.android.untilstipuha.modules.useCases.login

import com.cft.shift.android.untilstipuha.common.useCaseEngine.UseCase
import com.cft.shift.android.untilstipuha.modules.data.AuthInfoContainer
import com.cft.shift.android.untilstipuha.modules.data.Dao
import com.cft.shift.android.untilstipuha.modules.data.login.ILoginDao

class Login: UseCase<Login.RequestValues, Login.ResponseValues>() {

    override fun executeUseCase(requestValues: RequestValues) {
        Dao.instance.loginDao.tryLogin(
            requestValues.login,
            requestValues.password,
            object : ILoginDao.ITryLoginCallback {
                override fun onLogin(id: String?, token: String?) {
                    if (id != null && token != null) {
                        AuthInfoContainer.id = id
                        AuthInfoContainer.token = token
                        useCaseCallback.onSuccess(ResponseValues())
                    } else {
                        useCaseCallback.onError()
                    }
                }

            }
        )
    }

    class RequestValues(val login: String, val password: String): UseCase.RequestValues

    class ResponseValues: UseCase.ResponseValues
}