package com.cft.shift.android.untilstipuha.modules.userStories.detailedUserInfo

import com.cft.shift.android.untilstipuha.common.useCaseEngine.IUseCaseCallback
import com.cft.shift.android.untilstipuha.common.useCaseEngine.UseCaseHandler
import com.cft.shift.android.untilstipuha.modules.useCases.request.GetRequestByUserId
import com.cft.shift.android.untilstipuha.modules.useCases.request.GetRequests
import com.cft.shift.android.untilstipuha.modules.useCases.user.GetUserById

class DetailedUserInfoPresenter(override val view: IDetailedUserInfoView) : IDetailedUserInfoPresenter {

    override fun onLoadDetailedRequest() {
        val userId = view.getUserId()
        val loadRequests = GetRequestByUserId()
        val requests = GetRequestByUserId.RequestValues(userId)
        UseCaseHandler.instance.execute(
            loadRequests,
            requests,
            object: IUseCaseCallback<GetRequestByUserId.ResponseValues> {
                override fun onSuccess(response: GetRequestByUserId.ResponseValues) {
                    view.onListRequestsLoaded(response.request)
                }
                override fun onError() {
                    view.onError()
                }
            }
        )
    }

    override fun onLoadDetailedInfo() {
        val userId = view.getUserId()
        val getUserById = GetUserById()
        val getUserByIdParams = GetUserById.RequestValues(userId)

        UseCaseHandler.instance.execute(getUserById, getUserByIdParams, object: IUseCaseCallback<GetUserById.ResponseValues> {
            override fun onSuccess(response: GetUserById.ResponseValues) {
                view.onUpdateViewsWithUser(response.user)
            }
            override fun onError() {
                view.onError()
            }
        })
    }


    override fun start() {
        onLoadDetailedInfo()
        onLoadDetailedRequest()
    }
}