package com.cft.shift.android.untilstipuha.modules.useCases.request

import com.cft.shift.android.untilstipuha.common.useCaseEngine.UseCase
import com.cft.shift.android.untilstipuha.models.Request
import com.cft.shift.android.untilstipuha.modules.data.Dao
import com.cft.shift.android.untilstipuha.modules.data.request.IRequestDao

class GetRequestByUserId: UseCase<GetRequestByUserId.RequestValues, GetRequestByUserId.ResponseValues>() {
    override fun executeUseCase(requestValues: RequestValues) {
        Dao.instance.requestDao.getRequestByUserId(
            userId = requestValues.userId,
            callback = object: IRequestDao.IGetRequestsByUserIDCallback {
                override fun onRequestsLoaded(requests: List<Request>?) {
                    if (requests != null) {
                        useCaseCallback.onSuccess(ResponseValues(requests))
                    } else {
                        useCaseCallback.onError()
                    }
                }
            }
        )
    }

    class RequestValues(val userId: String): UseCase.RequestValues

    class ResponseValues(val request: List<Request>): UseCase.ResponseValues
}