package com.cft.shift.android.untilstipuha.modules.userStories.navigation.fragments.myself

import com.cft.shift.android.untilstipuha.common.useCaseEngine.IUseCaseCallback
import com.cft.shift.android.untilstipuha.common.useCaseEngine.UseCaseHandler
import com.cft.shift.android.untilstipuha.models.Request
import com.cft.shift.android.untilstipuha.modules.useCases.request.CloseRequest
import com.cft.shift.android.untilstipuha.modules.useCases.request.Deposit
import com.cft.shift.android.untilstipuha.modules.useCases.request.GetMyRequests
import com.cft.shift.android.untilstipuha.modules.useCases.request.Withdraw
import com.cft.shift.android.untilstipuha.modules.useCases.user.GetMyUser

class MyselfPresenter(override val view: IMyselfView) : IMyselfPresenter {
    override fun onDeposit(value: Double) {
        val deposit = Deposit()
        val rv = Deposit.RequestValues(value)
        UseCaseHandler.instance.execute(
            deposit,
            rv,
            object : IUseCaseCallback<Deposit.ResponseValues>{
                override fun onSuccess(response: Deposit.ResponseValues) {
                    onLoadMyselfInfo()
                }
                override fun onError() {
                    view.onError("Cannot deposit")
                }
            }
        )
    }

    override fun onWithdraw(value: Double) {
        val withdraw = Withdraw()
        val rv = Withdraw.RequestValues(value)
        UseCaseHandler.instance.execute(
            withdraw,
            rv,
            object : IUseCaseCallback<Withdraw.ResponseValues>{
                override fun onSuccess(response: Withdraw.ResponseValues) {
                    onLoadMyselfInfo()
                }

                override fun onError() {
                    view.onError("Cannot withdraw")
                }
            }
        )
    }

    override fun onCloseRequest(request: Request) {
        val closeRequest = CloseRequest()
        UseCaseHandler.instance.execute(
            closeRequest,
            CloseRequest.RequestValues(request),
            object : IUseCaseCallback<CloseRequest.ResponseValues> {
                override fun onSuccess(response: CloseRequest.ResponseValues) {
                    onLoadMyselfInfo()
                    onLoadMyselfRequest()
                    view.onSuccess("Request closed")
                }

                override fun onError() {
                    view.onError("Cannot close request")
                }
            }
        )
    }

    override fun onLoadMyselfRequest() {
        val loadRequests = GetMyRequests()
        val requests = GetMyRequests.RequestValues()
        UseCaseHandler.instance.execute(
            loadRequests,
            requests,
            object: IUseCaseCallback<GetMyRequests.ResponseValues> {
                override fun onSuccess(response: GetMyRequests.ResponseValues) {
                    view.onListRequestsLoaded(response.requests)
                }
                override fun onError() {
                    view.onError("Cannot load requests")
                }
            }
        )
    }

    override fun onLoadMyselfInfo() {
        val loadRequests = GetMyUser()
        val requests = GetMyUser.RequestValues()
        UseCaseHandler.instance.execute(
            loadRequests,
            requests,
            object: IUseCaseCallback<GetMyUser.ResponseValues> {
                override fun onSuccess(response: GetMyUser.ResponseValues) {
                    view.onUpdateViewsWithUser(response.me)
                }
                override fun onError() {
                    view.onError("Cannot load profile")
                }
            }
        )
    }

    override fun start() {
        onLoadMyselfInfo()
        onLoadMyselfRequest()
    }
}