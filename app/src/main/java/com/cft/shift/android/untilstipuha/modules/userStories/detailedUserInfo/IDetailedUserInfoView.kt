package com.cft.shift.android.untilstipuha.modules.userStories.detailedUserInfo

import com.cft.shift.android.untilstipuha.models.Request
import com.cft.shift.android.untilstipuha.models.User
import com.cft.shift.android.untilstipuha.modules.userStories.base.IBaseView

interface IDetailedUserInfoView: IBaseView<IDetailedUserInfoPresenter> {
    fun getUserId(): String
    fun onUpdateViewsWithUser(user: User)
    fun onListRequestsLoaded(requests: List<Request>)

    fun onError()
}