package com.cft.shift.android.untilstipuha.modules.userStories.navigation.fragments.userslist

import com.cft.shift.android.untilstipuha.models.User
import com.cft.shift.android.untilstipuha.modules.userStories.base.IBaseView

interface IUsersListView: IBaseView<IUsersListPresenter> {
    fun onListUsersLoaded(users: List<User>)
    fun onError()
}