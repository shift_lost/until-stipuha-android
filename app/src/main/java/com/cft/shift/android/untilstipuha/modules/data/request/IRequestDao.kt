package com.cft.shift.android.untilstipuha.modules.data.request

import com.cft.shift.android.untilstipuha.models.Request

interface IRequestDao {
    interface IAddRequestCallback {
        fun onRequestAdded(request: Request?)
    }

    interface IGetRequestsCallback {
        fun onRequestsLoaded(requests: List<Request>?)
    }

    interface IGetRequestsByUserIDCallback {
        fun onRequestsLoaded(requests: List<Request>?)
    }

    interface IGetRequestCallback {
        fun onRequestLoaded(request: Request?)
    }

    interface IDonateRequestByIdCallback {
        fun onRequestDonated(success: Boolean)
    }

    interface ICloseRequestCallback {
        fun onRequestClosed(success: Boolean)
    }

    fun addRequest(requestName: String, description: String, aim: Double, callback: IAddRequestCallback)

    fun getRequests(callback: IGetRequestsCallback)

    fun getRequestByUserId(userId: String, callback: IGetRequestsByUserIDCallback)

    fun getRequestById(requestId: String, callback: IGetRequestCallback)

    fun donateRequestById(requestId: String, value: Double, callback: IDonateRequestByIdCallback)

    fun closeRequest(requestId: String, callback: ICloseRequestCallback)
}