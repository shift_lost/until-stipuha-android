package com.cft.shift.android.untilstipuha.common.useCaseEngine

interface IUseCaseCallback<T> {
    fun onSuccess(response: T)
    fun onError()
}