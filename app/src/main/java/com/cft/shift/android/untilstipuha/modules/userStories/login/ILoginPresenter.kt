package com.cft.shift.android.untilstipuha.modules.userStories.login

import com.cft.shift.android.untilstipuha.modules.userStories.base.IBasePresenter

interface ILoginPresenter: IBasePresenter {
    val view: ILoginView
    fun validatePassword(password: String): Boolean
    fun onLogin(login: String, password: String)
}