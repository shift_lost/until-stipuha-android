package com.cft.shift.android.untilstipuha.modules.userStories.base

interface IBasePresenter {
    fun start()
}