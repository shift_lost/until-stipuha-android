package com.cft.shift.android.untilstipuha.modules.userStories.newRequest

import com.cft.shift.android.untilstipuha.modules.userStories.base.IBasePresenter

interface INewRequestPresenter: IBasePresenter {
    val view: INewRequestView

    fun onCreateRequest(name: String, aim: Double, description: String)
}