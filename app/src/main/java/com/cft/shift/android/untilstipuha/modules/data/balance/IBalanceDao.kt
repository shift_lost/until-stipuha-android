package com.cft.shift.android.untilstipuha.modules.data.balance

interface IBalanceDao {

    interface IWithdrawCallback {
        fun onWithdrew(success: Boolean)
    }

    interface IDepositCallback {
        fun onDeposited(success: Boolean)
    }

    /**
     * Removes money to the balance. Money will return to your credit card, ie.
     *
     * @param value amount of money to withdraw
     * @param callback callback which will be executed on the end of operation
     */
    fun withdraw(value: Double, callback: IWithdrawCallback)

    /**
     * Adds money from the balance. Money will be added from your credit card, ie.
     *
     * @param value amount of money to deposit
     * @param callback callback which will be executed on the end of operation
     */
    fun deposit(value: Double, callback: IDepositCallback)
}