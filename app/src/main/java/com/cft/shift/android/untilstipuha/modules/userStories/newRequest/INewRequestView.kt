package com.cft.shift.android.untilstipuha.modules.userStories.newRequest

import com.cft.shift.android.untilstipuha.modules.userStories.base.IBaseView

interface INewRequestView: IBaseView<INewRequestPresenter> {
    fun onRequestAdded()
    fun onError()
}