package com.cft.shift.android.untilstipuha.modules.useCases.user

import com.cft.shift.android.untilstipuha.common.useCaseEngine.UseCase
import com.cft.shift.android.untilstipuha.models.User
import com.cft.shift.android.untilstipuha.modules.data.Dao
import com.cft.shift.android.untilstipuha.modules.data.user.IUserDao

class GetUsers: UseCase<GetUsers.RequestValues, GetUsers.ResponseValues>() {
    override fun executeUseCase(requestValues: RequestValues) {
        Dao.instance.userDao.getUsers(object: IUserDao.IGetUsersCallback {
            override fun onUsersLoaded(users: List<User>?) {
                when {
                    users != null -> useCaseCallback.onSuccess(ResponseValues(users))
                    else -> useCaseCallback.onError()
                }
            }
        })
    }

    class RequestValues: UseCase.RequestValues

    class ResponseValues(val users: List<User>): UseCase.ResponseValues
}