package com.cft.shift.android.untilstipuha.modules.userStories.login

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.cft.shift.android.untilstipuha.R
import com.cft.shift.android.untilstipuha.common.SharedConstants
import com.cft.shift.android.untilstipuha.modules.userStories.detailedUserInfo.DetailedUserInfoActivity
import com.cft.shift.android.untilstipuha.modules.userStories.navigation.NavigationActivity
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity: AppCompatActivity(), ILoginView {
    private lateinit var presenter: ILoginPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val presenter = LoginPresenter(this)
        setPresenter(presenter)

        loginButton.setOnClickListener {
            val login = loginField.text.toString()
            val password = passwordField.text.toString()
            when {
                presenter.validatePassword(password) -> presenter.onLogin(login, password)
                else -> showErrorScreen("Password is not strong enough!")
            }
        }

    }

    override fun setPresenter(presenter: ILoginPresenter) {
        this.presenter = presenter
    }

    override fun showErrorScreen(message: String) {
        Toasty.error(this, message, Toasty.LENGTH_LONG).show()
    }

    override fun onSuccessLogin() {
        Toasty.success(this, "Success login!", Toasty.LENGTH_SHORT).show()

        //testing code
        val intent = Intent(this, NavigationActivity::class.java)
        startActivity(intent)
    }
}