package com.cft.shift.android.untilstipuha.modules.useCases.request

import com.cft.shift.android.untilstipuha.common.useCaseEngine.UseCase
import com.cft.shift.android.untilstipuha.modules.data.Dao
import com.cft.shift.android.untilstipuha.modules.data.balance.IBalanceDao

class Withdraw: UseCase<Withdraw.RequestValues, Withdraw.ResponseValues>() {
    override fun executeUseCase(requestValues: RequestValues) {
        Dao.instance.balanceDao.withdraw(requestValues.value, object : IBalanceDao.IWithdrawCallback{
            override fun onWithdrew(success: Boolean) {
                if (success){
                    useCaseCallback.onSuccess(ResponseValues())
                } else {
                    useCaseCallback.onError()
                }
            }
        })
    }

    class RequestValues(val value: Double): UseCase.RequestValues

    class ResponseValues: UseCase.ResponseValues
}