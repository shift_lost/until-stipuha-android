package com.cft.shift.android.untilstipuha.modules.data.request

import com.cft.shift.android.untilstipuha.models.network.requests.RequestCreation
import com.cft.shift.android.untilstipuha.modules.data.AuthInfoContainer
import com.cft.shift.android.untilstipuha.modules.data.validateToken
import com.cft.shift.android.untilstipuha.modules.network.ApiService
import java.io.IOException
import java.lang.Exception

class NetworkRequestDao: IRequestDao {
    override fun addRequest(
        requestName: String,
        description: String,
        aim: Double,
        callback: IRequestDao.IAddRequestCallback
    ) {
        AuthInfoContainer.validateToken {
            callback.onRequestAdded(null)
            return
        }

        val request = RequestCreation(requestName, description, aim)

        try {
            val response = ApiService.instance.createRequest(AuthInfoContainer.token, request).execute()
            callback.onRequestAdded(response.body())
        } catch (e: IOException) {
            callback.onRequestAdded(null)
        }
    }

    override fun getRequests(callback: IRequestDao.IGetRequestsCallback) {
        try {
            val response = ApiService.instance.getRequestsList().execute()
            callback.onRequestsLoaded(response?.body())
        } catch (e: IOException) {
            callback.onRequestsLoaded(null)
        }
    }

    override fun getRequestById(requestId: String, callback: IRequestDao.IGetRequestCallback) {
        try {
            val response = ApiService.instance.getRequestByID(requestId).execute()
            callback.onRequestLoaded(response?.body())
        } catch (e: IOException) {
            callback.onRequestLoaded(null)
        }
    }

    override fun getRequestByUserId(userId: String, callback: IRequestDao.IGetRequestsByUserIDCallback) {
        try {
            val response = ApiService.instance.getRequestListByUserID(userId).execute()
            callback.onRequestsLoaded(response?.body())
        } catch (e: IOException) {
            callback.onRequestsLoaded(null)
        }
    }

    override fun donateRequestById(requestId: String, value: Double, callback: IRequestDao.IDonateRequestByIdCallback) {
        AuthInfoContainer.validateToken {
            callback.onRequestDonated(false)
            return
        }

        try {
            val response = ApiService.instance.donateToRequest(AuthInfoContainer.token, requestId, value).execute()
            callback.onRequestDonated(response.isSuccessful)
        } catch (e: IOException) {
            callback.onRequestDonated(false)
        }
    }

    override fun closeRequest(requestId: String, callback: IRequestDao.ICloseRequestCallback) {
        AuthInfoContainer.validateToken {
            callback.onRequestClosed(false)
            return
        }
        try {
            val response = ApiService.instance.deleteRequest(AuthInfoContainer.token, requestId).execute()
            callback.onRequestClosed(response?.isSuccessful == true)
        } catch (ex: Exception){
            callback.onRequestClosed(false)
        }
    }
}