package com.cft.shift.android.untilstipuha.modules.userStories.navigation.fragments.requestslist

import com.cft.shift.android.untilstipuha.models.Request
import com.cft.shift.android.untilstipuha.modules.userStories.base.IBaseView

interface IRequestsListView: IBaseView<IRequestsListPresenter> {
    fun onListRequestsLoaded(requests: List<Request>)
    fun onError()
}