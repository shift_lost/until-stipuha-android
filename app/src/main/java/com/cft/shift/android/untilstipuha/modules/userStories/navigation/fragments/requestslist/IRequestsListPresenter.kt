package com.cft.shift.android.untilstipuha.modules.userStories.navigation.fragments.requestslist

import com.cft.shift.android.untilstipuha.modules.userStories.base.IBasePresenter

interface IRequestsListPresenter: IBasePresenter {
    val view: IRequestsListView

    fun onLoadRequests()
}