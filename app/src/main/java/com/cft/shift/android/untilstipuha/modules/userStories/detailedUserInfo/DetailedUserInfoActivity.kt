package com.cft.shift.android.untilstipuha.modules.userStories.detailedUserInfo

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.cft.shift.android.untilstipuha.R
import com.cft.shift.android.untilstipuha.common.SharedConstants
import com.cft.shift.android.untilstipuha.models.Request
import com.cft.shift.android.untilstipuha.models.User
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.activity_view_profile.*
import kotlin.math.roundToInt

class DetailedUserInfoActivity: AppCompatActivity(), IDetailedUserInfoView {
    private lateinit var presenter: IDetailedUserInfoPresenter
    private lateinit var list: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_profile)
        setPresenter(DetailedUserInfoPresenter(this))

        list = viewRequestList
        list.layoutManager = LinearLayoutManager(this)

        presenter.start()
    }

    override fun getUserId(): String {
        return intent.getStringExtra(SharedConstants.userIdTag)
    }

    override fun onUpdateViewsWithUser(user: User) {
        viewUsername.text = user.name
        viewKarmaText.text = user.karma.toString()
        viewMaxRequestText.text = user.maxRequest.roundToInt().toString()
    }

    override fun onListRequestsLoaded(requests: List<Request>) {
        list.adapter = DetailedRequestsListAdapter(requests, this)
    }

    override fun onError() {
        Toasty.error(this, "Cannot load this user").show()
    }

    override fun setPresenter(presenter: IDetailedUserInfoPresenter) {
        this.presenter = presenter
    }
}