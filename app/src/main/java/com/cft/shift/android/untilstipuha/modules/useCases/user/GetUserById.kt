package com.cft.shift.android.untilstipuha.modules.useCases.user

import com.cft.shift.android.untilstipuha.common.useCaseEngine.UseCase
import com.cft.shift.android.untilstipuha.models.User
import com.cft.shift.android.untilstipuha.modules.data.Dao
import com.cft.shift.android.untilstipuha.modules.data.user.IUserDao

class GetUserById: UseCase<GetUserById.RequestValues, GetUserById.ResponseValues>() {
    override fun executeUseCase(requestValues: RequestValues) {
        Dao.instance.userDao.getUserById(requestValues.id, object: IUserDao.IGetUserByIdCallback {
            override fun onUserLoaded(user: User?) {
                when {
                    user != null -> useCaseCallback.onSuccess(GetUserById.ResponseValues(user))
                    else -> useCaseCallback.onError()
                }
            }
        })
    }

    class RequestValues(val id: String): UseCase.RequestValues

    class ResponseValues(val user: User): UseCase.ResponseValues
}