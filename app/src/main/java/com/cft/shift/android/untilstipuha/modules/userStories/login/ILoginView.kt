package com.cft.shift.android.untilstipuha.modules.userStories.login

import com.cft.shift.android.untilstipuha.modules.userStories.base.IBaseView

interface ILoginView: IBaseView<ILoginPresenter> {
    fun showErrorScreen(message: String)
    fun onSuccessLogin()
}