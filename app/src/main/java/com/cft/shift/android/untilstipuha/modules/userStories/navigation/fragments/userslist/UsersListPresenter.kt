package com.cft.shift.android.untilstipuha.modules.userStories.navigation.fragments.userslist

import android.util.Log
import com.cft.shift.android.untilstipuha.common.useCaseEngine.IUseCaseCallback
import com.cft.shift.android.untilstipuha.common.useCaseEngine.UseCaseHandler
import com.cft.shift.android.untilstipuha.modules.useCases.user.GetUsers

class UsersListPresenter(override val view: IUsersListView) : IUsersListPresenter {
    override fun onLoadUsers() {
        val loadUsers = GetUsers()
        val users = GetUsers.RequestValues()
        UseCaseHandler.instance.execute(
            loadUsers,
            users,
            object: IUseCaseCallback<GetUsers.ResponseValues> {
                override fun onSuccess(response: GetUsers.ResponseValues) {
                    Log.d("NewRequestPresenter", response.users.toString())
                    view.onListUsersLoaded(response.users)
                }
                override fun onError() {
                    view.onError()
                }
            }
        )
    }

    override fun start() {
        onLoadUsers()
    }
}