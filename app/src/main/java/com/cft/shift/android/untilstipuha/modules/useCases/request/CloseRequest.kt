package com.cft.shift.android.untilstipuha.modules.useCases.request

import com.cft.shift.android.untilstipuha.common.useCaseEngine.UseCase
import com.cft.shift.android.untilstipuha.models.Request
import com.cft.shift.android.untilstipuha.modules.data.Dao
import com.cft.shift.android.untilstipuha.modules.data.request.IRequestDao

class CloseRequest: UseCase<CloseRequest.RequestValues, CloseRequest.ResponseValues>() {
    override fun executeUseCase(requestValues: RequestValues) {
        Dao.instance.requestDao.closeRequest(requestValues.request.id, object : IRequestDao.ICloseRequestCallback {
            override fun onRequestClosed(success: Boolean) {
                if (success){
                    useCaseCallback.onSuccess(ResponseValues())
                } else {
                    useCaseCallback.onError()
                }
            }
        })
    }

    class RequestValues(val request: Request): UseCase.RequestValues

    class ResponseValues: UseCase.ResponseValues
}