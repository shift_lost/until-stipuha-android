package com.cft.shift.android.untilstipuha.common.useCaseEngine

class UseCaseHandler private constructor() {

    private val useCaseScheduler: IUseCaseScheduler

    init {
        useCaseScheduler = UseCaseThreadPoolScheduler()
    }

    private object Holder {
        val INSTANCE = UseCaseHandler()
    }

    companion object {
        val instance by lazy { Holder.INSTANCE }
    }

    fun <T: UseCase.RequestValues, R: UseCase.ResponseValues> execute(
        useCase: UseCase<T, R>,
        requestValues: T,
        callback: IUseCaseCallback<R>
    ){
        useCase.requestValues = requestValues
        useCase.useCaseCallback =
                UseCaseCallbackWrapper(callback, this)

        useCaseScheduler.execute(Runnable {
            useCase.run()
        })
    }

    fun <T: UseCase.ResponseValues> notifyResponse(response: T, useCaseCallback: IUseCaseCallback<T>){
        useCaseScheduler.onResponse(response, useCaseCallback)
    }

    fun <T: UseCase.ResponseValues> notifyError(useCaseCallback: IUseCaseCallback<T>){
        useCaseScheduler.onError(useCaseCallback)
    }
}