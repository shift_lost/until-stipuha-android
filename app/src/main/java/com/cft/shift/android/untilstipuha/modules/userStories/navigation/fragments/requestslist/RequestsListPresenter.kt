package com.cft.shift.android.untilstipuha.modules.userStories.navigation.fragments.requestslist

import android.util.Log
import com.cft.shift.android.untilstipuha.common.useCaseEngine.IUseCaseCallback
import com.cft.shift.android.untilstipuha.common.useCaseEngine.UseCaseHandler
import com.cft.shift.android.untilstipuha.modules.useCases.request.GetRequests

class RequestsListPresenter(override val view: IRequestsListView) : IRequestsListPresenter {
    override fun onLoadRequests() {
        val loadRequests = GetRequests()
        val requests = GetRequests.RequestValues()
        UseCaseHandler.instance.execute(
            loadRequests,
            requests,
            object: IUseCaseCallback<GetRequests.ResponseValues> {
                override fun onSuccess(response: GetRequests.ResponseValues) {
                    view.onListRequestsLoaded(response.requests)
                }
                override fun onError() {
                    view.onError()
                }
            }
        )
    }

    override fun start() {
        onLoadRequests()
    }
}