package com.cft.shift.android.untilstipuha.modules.useCases.request

import com.cft.shift.android.untilstipuha.common.useCaseEngine.UseCase
import com.cft.shift.android.untilstipuha.models.Request
import com.cft.shift.android.untilstipuha.modules.data.AuthInfoContainer
import com.cft.shift.android.untilstipuha.modules.data.Dao
import com.cft.shift.android.untilstipuha.modules.data.request.IRequestDao

class GetMyRequests: UseCase<GetMyRequests.RequestValues, GetMyRequests.ResponseValues>() {
    override fun executeUseCase(requestValues: RequestValues) {
        Dao.instance.requestDao.getRequestByUserId(
            userId = AuthInfoContainer.id,
            callback = object: IRequestDao.IGetRequestsByUserIDCallback {
                override fun onRequestsLoaded(requests: List<Request>?) {
                    if (requests != null) {
                        useCaseCallback.onSuccess(ResponseValues(requests))
                    } else {
                        useCaseCallback.onError()
                    }
                }
            }
        )
    }

    class RequestValues: UseCase.RequestValues

    class ResponseValues(val requests: List<Request>): UseCase.ResponseValues
}