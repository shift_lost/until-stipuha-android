package com.cft.shift.android.untilstipuha.modules.useCases.request

import com.cft.shift.android.untilstipuha.common.useCaseEngine.UseCase
import com.cft.shift.android.untilstipuha.models.Request
import com.cft.shift.android.untilstipuha.modules.data.Dao
import com.cft.shift.android.untilstipuha.modules.data.request.IRequestDao

class GetRequests: UseCase<GetRequests.RequestValues, GetRequests.ResponseValues>() {
    override fun executeUseCase(requestValues: RequestValues) {
        Dao.instance.requestDao.getRequests(callback = object: IRequestDao.IGetRequestsCallback {
            override fun onRequestsLoaded(requests: List<Request>?) {
                when {
                    requests != null -> useCaseCallback.onSuccess(
                        ResponseValues(
                            requests
                        )
                    )
                    else -> useCaseCallback.onError()
                }
            }
        })
    }

    class RequestValues: UseCase.RequestValues

    class ResponseValues(val requests: List<Request>): UseCase.ResponseValues
}