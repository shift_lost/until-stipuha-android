package com.cft.shift.android.untilstipuha.modules.data.login

import com.cft.shift.android.untilstipuha.models.network.requests.LoginRequest
import com.cft.shift.android.untilstipuha.models.network.responses.LoginResponse
import com.cft.shift.android.untilstipuha.modules.data.AuthInfoContainer
import com.cft.shift.android.untilstipuha.modules.network.ApiService
import retrofit2.Response
import java.io.IOException

class NetworkLoginDao: ILoginDao {

    override fun tryLogin(name: String, password: String, callback: ILoginDao.ITryLoginCallback) {
        val loginRequest = LoginRequest(name, password)

        val response: Response<LoginResponse> = try {
            ApiService.instance.loginUser(loginRequest).execute()
        } catch (e: IOException) {
            callback.onLogin(null, null)
            return
        }

        if (response.isSuccessful) {
            val loginResponse = response.body()
            if (loginResponse != null) {
                AuthInfoContainer.id = loginResponse.id
                AuthInfoContainer.token = loginResponse.token
                callback.onLogin(loginResponse.id, loginResponse.token)
            } else {
                callback.onLogin(null, null)
            }
        } else {
            callback.onLogin(null, null)
        }
    }
}