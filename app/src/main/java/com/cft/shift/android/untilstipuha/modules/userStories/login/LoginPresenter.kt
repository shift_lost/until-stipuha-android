package com.cft.shift.android.untilstipuha.modules.userStories.login

import com.cft.shift.android.untilstipuha.common.useCaseEngine.IUseCaseCallback
import com.cft.shift.android.untilstipuha.common.useCaseEngine.UseCaseHandler
import com.cft.shift.android.untilstipuha.modules.useCases.login.Login

class LoginPresenter(override val view: ILoginView) : ILoginPresenter {
    override fun validatePassword(password: String): Boolean {
        return password.length > 4
    }

    override fun onLogin(login: String, password: String) {
        val request = Login()
        val requestValues = Login.RequestValues(login, password)
        UseCaseHandler.instance.execute(
            request,
            requestValues,
            object: IUseCaseCallback<Login.ResponseValues> {
                override fun onSuccess(response: Login.ResponseValues) {
                    view.onSuccessLogin()
                }

                override fun onError() {
                    view.showErrorScreen("Error on login")
                }
            }

        )
    }

    override fun start() {}
}