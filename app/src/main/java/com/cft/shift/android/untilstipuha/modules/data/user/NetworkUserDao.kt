package com.cft.shift.android.untilstipuha.modules.data.user

import android.util.Log
import com.cft.shift.android.untilstipuha.modules.data.AuthInfoContainer
import com.cft.shift.android.untilstipuha.modules.data.validateToken
import com.cft.shift.android.untilstipuha.modules.network.ApiService
import java.io.IOException

class NetworkUserDao: IUserDao {
    override fun getMe(callback: IUserDao.IGetMeCallback) {
        AuthInfoContainer.validateToken {
            Log.d("ME_LOAD", "TOKEN VALIDATION")
            callback.onMeLoaded(null)
            return
        }
        try {
            val response = ApiService.instance.getMyInfo(AuthInfoContainer.token, AuthInfoContainer.id).execute()
            callback.onMeLoaded(response?.body())
        } catch (e: Exception) {
            Log.d("ME_LOAD", "EXCEPTION")
            callback.onMeLoaded(null)
        }
    }

    override fun getUserById(id: String, callback: IUserDao.IGetUserByIdCallback) {
        try {
            val response = ApiService.instance.getUserInfo(id).execute()
            callback.onUserLoaded(response?.body())
        } catch (e: IOException) {
            callback.onUserLoaded(null)
        }
    }

    override fun getUsers(callback: IUserDao.IGetUsersCallback) {
        try {
            val response = ApiService.instance.getUserList().execute()
            callback.onUsersLoaded(response?.body())
        } catch (e: IOException) {
            callback.onUsersLoaded(null)
        }
    }
}