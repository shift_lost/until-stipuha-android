package com.cft.shift.android.untilstipuha.modules.data

object AuthInfoContainer {
    lateinit var id: String
    lateinit var token: String

//    fun validateToken(block: () -> Unit): AuthInfoContainer {
//        if (!AuthInfoContainer::token.isInitialized) block()
//        return this
//    }
}

inline fun AuthInfoContainer.validateToken(block: () -> Unit) {
    try {
        AuthInfoContainer.token
    } catch (e: UninitializedPropertyAccessException) {
        block()
    }
}