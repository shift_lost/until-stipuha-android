package com.cft.shift.android.untilstipuha.models

data class Request(
    val id: String,
    val name: String,
    val description: String,
    val authorID: String,
    val aim: Double,
    val balance: Double
)